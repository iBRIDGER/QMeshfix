﻿#if QT_VERSION_MAJOR > 4
    #if _MSC_VER >= 1600
        #pragma execution_character_set("utf-8")
    #endif
#endif

#include "Meshfix/triangle3d.h"
//#include "geometricfunctions.h"
#include <QDebug>

Triangle3D::Triangle3D()
{
    int i;
    normal*=0.0;//make the normal 0,0,0

    for(i=0; i < 3; i++)
    {
        vertex[i]*= 0.0;//make the vertex all reside on 0,0,0
    }

    maxBound.setX(-99999999.0);
    maxBound.setY(-99999999.0);
    maxBound.setZ(-99999999.0);

    minBound.setX(99999999.0);
    minBound.setY(99999999.0);
    minBound.setZ(99999999.0);

}

Triangle3D::~Triangle3D()
{

}


void Triangle3D::UpdateBounds()
{
    int i;

    //reset the bounds:
    maxBound.setX(-99999999.0);
    maxBound.setY(-99999999.0);
    maxBound.setZ(-99999999.0);

    minBound.setX(99999999.0);
    minBound.setY(99999999.0);
    minBound.setZ(99999999.0);

    //遍历顶点看最大和最小XYZ值
    for(i=0; i < 3; i++)
    {
        //max
        if(vertex[i].x() > maxBound.x())
        {
            maxBound.setX(vertex[i].x());
        }
        if(vertex[i].y() > maxBound.y())
        {
            maxBound.setY(vertex[i].y());
        }
        if(vertex[i].z() > maxBound.z())
        {
            maxBound.setZ(vertex[i].z());
        }

        //min
        if(vertex[i].x() < minBound.x())
        {
            minBound.setX(vertex[i].x());
        }
        if(vertex[i].y() < minBound.y())
        {
            minBound.setY(vertex[i].y());
        }
        if(vertex[i].z() < minBound.z())
        {
            minBound.setZ(vertex[i].z());
        }
    }


}
void Triangle3D::UpdateNormalFromGeom()
{
    normal = QVector3D::crossProduct(vertex[1] - vertex[0], vertex[2] - vertex[0]);
    normal.normalize();
}

bool Triangle3D::IsBad()
{
    //判断三角面上下界的差值是否为0，表示面有问题，这里不严谨
    //TODO: 需要注意，不严谨
//    double d = Distance3D(maxBound,minBound);
//    if(IsZero(d,0.00001))//for possible double error.
//    {
//        return true;
//    }
    return false;
}

bool Triangle3D::IntersectsXYPlane(double realAltitude)
{
    if(IsBad() || ParallelXYPlane())
    {	//面
        return false;
    }
    if(maxBound.z() > realAltitude && minBound.z() <= realAltitude)
    {
        return true;
    }

    //the triangle is not in bounds at all:
    return false;
}

void Triangle3D::draw()
{
 //TODO: 待完善
}

bool Triangle3D::ParallelXYPlane()
{
    if((vertex[0].z() == vertex[1].z())&&(vertex[0].z() == vertex[2].z()))
    {
        return true;
    }
    return false;
}
