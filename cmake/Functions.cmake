function(AddSources rel_path group_name list_src)
message(STATUS "AddSources --->   > ${rel_path} >> ${group_name}")
include_directories(${rel_path})
set(list_globbed_src "")
#遍历所有子目录
file(GLOB_RECURSE  ALL_SOURCE_FILES LIST_DIRECTORIES true "${rel_path}/*.h" "${rel_path}/*.cpp" "${rel_path}/*.cxx" "${rel_path}/*.c" "${rel_path}/*.hpp")
# 将文件按文件夹分组
foreach(SOURCE_FILE ${ALL_SOURCE_FILES})
    if(IS_DIRECTORY ${SOURCE_FILE})
        include_directories(${SOURCE_FILE})
        message(STATUS "File found: ${SOURCE_FILE}")
        # 获取文件的相对路径
        file(RELATIVE_PATH RELATIVE_PATH ${SOURCE_DIR} ${SOURCE_FILE})

        # 计算文件所在的文件夹
        string(REPLACE "/" "\\" GROUP_NAME ${RELATIVE_PATH})
        message(STATUS "GROUP_NAME: ${GROUP_NAME}")

        file(GLOB  list_globbed  "${SOURCE_FILE}/*.h" "${SOURCE_FILE}/*.cpp" "${SOURCE_FILE}/*.cxx" "${SOURCE_FILE}/*.c" "${SOURCE_FILE}/*.hpp")
        list(APPEND list_globbed_src ${list_globbed})
        # 将文件添加到对应的源组
        source_group("${GROUP_NAME}" FILES ${list_globbed})
        message("sb-----------------------------------:${list_globbed}")
    endif()
endforeach()
set(list_return ${list_globbed_src} ${${list_src}})
set(${list_src} ${list_return} PARENT_SCOPE)
#source_group(${group_name} FILES ${list_globbed})



endfunction()

function(AddResources rel_path group_name list_res)
  message(STATUS "AddResources --> ${rel_path} >> ${group_name}")
  file(GLOB_RECURSE list_globbed "${rel_path}/*.qml" "${rel_path}/*.png" "${rel_path}/*.ttf" "${rel_path}/*.js" "${rel_path}/*.json" "${rel_path}/*.cfg")
  foreach(file ${list_globbed})
    message(STATUS "     - ${file}")
  endforeach()
  source_group(${group_name} FILES ${list_globbed})
  set(list_return ${list_globbed} ${${list_res}})
  set(${list_res} ${list_return} PARENT_SCOPE)
endfunction()

function(AddResourcesQRC rel_path group_name list_res)
  message(STATUS "AddQRC --> ${rel_path} >> ${group_name}")
  file(GLOB list_globbed "${rel_path}/*.qrc")
  foreach(file ${list_globbed})
    message(STATUS "     - ${file}")
  endforeach()
  source_group(${group_name} FILES ${list_globbed})
  set(list_return ${list_globbed} ${${list_res}})
  set(${list_res} ${list_return} PARENT_SCOPE)
endfunction()
