#-------------------------------------------------
#
# Project created by QtCreator 2017-12-13T23:17:47
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11
INCLUDEPATH += $$PWD
message($$PWD)
#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#TARGET = QMeshFix
#TEMPLATE = app
#DESTDIR += ./bin #指定应用程序放置目录
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


DEFINES += MODEL_TRIANGLE3D

DEFINES += FUNC_MESHFIX

SOURCES += \
    $$PWD/src/Algorithms/checkAndRepair.cpp \
    $$PWD/src/Algorithms/detectIntersections.cpp \
    $$PWD/src/Algorithms/holeFilling.cpp \
    $$PWD/src/Algorithms/marchIntersections.cpp \
    $$PWD/src/Algorithms/subdivision.cpp \
    $$PWD/src/Kernel/coordinates.cpp \
    $$PWD/src/Kernel/graph.cpp \
    $$PWD/src/Kernel/heap.cpp \
    $$PWD/src/Kernel/jqsort.cpp \
    $$PWD/src/Kernel/list.cpp \
    $$PWD/src/Kernel/matrix.cpp \
    $$PWD/src/Kernel/point.cpp \
    $$PWD/src/Kernel/tmesh.cpp \
    $$PWD/src/Kernel/orientation.c \
    $$PWD/src/MeshFix/meshfix.cpp \    
    $$PWD/src/TMesh/edge.cpp \
    $$PWD/src/TMesh/io.cpp \
    $$PWD/src/TMesh/tin.cpp \
    $$PWD/src/TMesh/triangle.cpp \
    $$PWD/src/TMesh/vertex.cpp
    #$$PWD/src/MeshFix/triangle3d.cpp

HEADERS += \
        #mainwindow.h \
    #src/MeshFix/triangle3d.h \
    $$PWD/include/Kernel/basics.h \
    $$PWD/include/Kernel/coordinates.h \
    $$PWD/include/Kernel/graph.h \
    $$PWD/include/Kernel/heap.h \
    $$PWD/include/Kernel/jqsort.h \
    $$PWD/include/Kernel/list.h \
    $$PWD/include/Kernel/matrix.h \
    $$PWD/include/Kernel/point.h \
    $$PWD/include/Kernel/tmesh_kernel.h \
    $$PWD/include/TMesh/detectIntersections.h \
    $$PWD/include/TMesh/edge.h \
    $$PWD/include/TMesh/marchIntersections.h \
    $$PWD/include/TMesh/tin.h \
    $$PWD/include/TMesh/tmesh.h \
    $$PWD/include/TMesh/triangle.h \
    $$PWD/include/TMesh/vertex.h  \
    $$PWD/include/MeshFix/meshfix.h
#FORMS +=        # $$PWD/mainwindow.ui

INCLUDEPATH += \
        $$PWD/./include \
        $$PWD/./include/Kernel \
        $$PWD/./include/TMesh \
        $$PWD/../PocketMaker_APP/core/QGLGraphicsViewer/Model \
        $$PWD/./include/MeshFix


