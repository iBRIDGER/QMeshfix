

#ifdef __cpp_lambdas

    extern "C" {
#endif

    void initPredicates();

    //! Orientation predicates using filtering on doubles
    double orient2d(double *, double *, double *);
    double orient3d(double *, double *, double *, double *);


#ifdef __cpp_lambdas
    }
#endif


