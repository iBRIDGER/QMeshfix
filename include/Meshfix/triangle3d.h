﻿#ifndef TRIANGLE3D_H
#define TRIANGLE3D_H


#include <QVector3D>

/**
 * @brief The Triangle3D class 保存三角面数据的类 法向量 边界
 */
class Triangle3D
{
public:
    QVector3D normal; //向量
    //NOTE: 这里的点数据有冗余,可以利用CuraEngine的方法简化
    QVector3D vertex[3]; // 3个顶点

    //specs
    QVector3D maxBound; //下边界
    QVector3D minBound; //上边界

    Triangle3D();
    ~Triangle3D();

    void UpdateBounds(); //更新边界 遍历所有的点
    void UpdateNormalFromGeom();// 由顶点算出法向量 recomputes normal from vertex's
    bool IsBad(); // 三角形没有边界或者面积表示坏面 returns true if the triangle has no "area" or no bounds.
    bool ParallelXYPlane(); //三角面平行于XY平面
    bool IntersectsXYPlane(double realAltitude); //三角面与一定高度的XY平面相交

    /**
     * @brief 绘制三角形
     */
    void draw();

    /**
     * @brief GreaterTopAltitude 比较谁高
     * @param t1
     * @param t2
     * @return
     */
    static bool GreaterTopAltitude(Triangle3D* t1, Triangle3D* t2)
    {
        return (t1->maxBound.z() < t2->maxBound.z());
    }

    /**
     * @brief GreaterBottomAltitude //比较谁低 排序用
     * @param t1
     * @param t2
     * @return
     */
    static bool GreaterBottomAltitude(Triangle3D* t1, Triangle3D* t2)
    {
        return (t1->minBound.z() < t2->minBound.z());
    }
};

#endif // TRIANGLE3D_H
