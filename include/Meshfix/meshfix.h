﻿#ifndef MESHFIX_H
#define MESHFIX_H


#ifdef MODEL_TRIANGLE3D

#include "TMesh/tmesh.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "MeshFix/triangle3d.h"
std::vector<Triangle3D>* meshfix(const std::vector<Triangle3D> *triList,bool skip = false,bool join=false,bool output=false);

/**
* @brief用于测试没有实际用途
*/
int saveSTL(const std::vector<Triangle3D> &triList,const char *fname);
#endif
#endif // MESHFIX_H
